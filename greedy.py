coins = [30, 24, 12, 6, 3, 1]
# for arbitrary coin systems, though: if the coin denominations were 1, 3 and 4, then to make 6,
# the greedy algorithm would choose three coins (4,1,1) whereas the optimal solution is two coins (3,3).
s = 48

coins_returned = []

temp = 0

while temp != s:
    for coin in coins:
        if s-temp-coin >= 0:
            temp += coin
            coins_returned.append(coin)
            break

print coins_returned
print [24, 24]