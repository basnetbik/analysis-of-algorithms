from __future__ import division

n = 5
W = 100

w = {
    i: i for i in range(1, 10)
}

w = {
    1: 10,
    2: 20,
    3: 30,
    4: 40,
    5: 50
}

v = {
    1: 20,
    2: 30,
    3: 66,
    4: 40,
    5: 60
}

x = {i: 0 for i in range(1, n+1)}

vw = sorted([v[i]/w[i] for i in range(1, n+1)], reverse=True)
vw_map = dict()

for z in range(1, n+1):
    vw_map[v[z]/w[z]] = z

values = []
weight = 0
index = 0
while weight < W:
    i = vw_map[vw[index]]
    index += 1

    if weight + w[i] <= W:
        exec "x[i] += 1\nvalues.append((1, v[i]))\nweight += w[i]"
    else:
        exec "x[i] = (W - weight)/w[i]\nvalues.append((x[i], v[i]*x[i]))\nweight = W"

# print vw
# print vw_map
# print w
print x
print values


