import copy
import math
import pprint


board_size = 8


def lateralus(pause=45):
    board = [['-'] * board_size for _ in range(board_size)]

    # y, x = 0, 0
    # y, x = 0, 3
    # y, x = 0, 4
    # y, x = 0, 5
    # y, x = 0, 6
    # y, x = 0, 7
    # y, x = 1, 3
    # y, x = 1, 4
    # y, x = 1, 7
    # y, x = 2, 0
    # y, x = 2, 3
    # y, x = 2, 4
    # y, x = 2, 6
    # y, x = 3, 0
    # y, x = 3, 1
    # y, x = 3, 2
    # y, x = 3, 4
    # y, x = 3, 5
    # y, x = 3, 6
    # y, x = 3, 7
    # y, x = 4, 0
    # y, x = 4, 1
    y, x = 4, 2
    # y, x = 4, 5
    # y, x = 4, 6
    # y, x = 4, 7
    # y, x = 5, 2
    # y, x = 5, 3
    # y, x = 5, 4
    # y, x = 5, 5
    # y, x = 5, 7
    # y, x = 6, 3
    # y, x = 6, 4
    # y, x = 7, 0
    # y, x = 7, 2
    # y, x = 7, 3
    # y, x = 7, 4
    # y, x = 7, 7

    # todo make it converge from all/most starting point

    last_x, last_y = x, y

    oy, ox = 40, 40

    def is_valid(space):
        return 0 <= space[0] < 8 and 0 <= space[1] < 8 and board[space[1]][space[0]] == '-'

    for i in range(1, 65):
        val = str(i)
        if len(val) == 1:
            val = f'0{val}'

        board[y][x] = val
        pprint.pprint(board)
        print('\n')

        if i == pause:
            return board, x, y, True

        if i == 64:
            print('Converged!!!')
            break

        possible_x = ((x + 1), (x + 2), (x + 2), (x + 1), (x - 1), (x - 2), (x - 2), (x - 1))
        possible_y = ((y - 2), (y - 1), (y + 1), (y + 2), (y + 2), (y + 1), (y - 1), (y - 2))
        possible_spaces = zip(possible_x, possible_y)
        possible_spaces = list(filter(is_valid, possible_spaces))

        if not possible_spaces:
            break

        angle = math.degrees(math.atan2(-(y * 10 + 5 - oy), (x * 10 + 5 - ox)))
        delta_x, delta_y = x - last_x, y - last_y

        next_x, next_y = possible_spaces[0]
        new_angle = math.degrees(math.atan2((-(next_y * 10 + 5 - oy)), (next_x * 10 + 5 - ox)))
        chosen_diff = new_angle - angle
        chosen_x, chosen_y = next_x, next_y

        if all([
            abs(last_y * 10 + 5 - oy) + abs(last_x * 10 + 5 - ox) < 15,
            abs(next_y * 10 + 5 - oy) + abs(next_x * 10 + 5 - ox) < 15
        ]):
            last_x, last_y = x, y
            x, y = chosen_x, chosen_y
            continue

        for (next_x, next_y) in possible_spaces[1:]:
            if (chosen_x, chosen_y) in ((0, 0), (0, 7), (7, 0), (7, 7)):
                break

            new_angle = math.degrees(math.atan2((-(next_y * 10 + 5 - oy)), (next_x * 10 + 5 - ox)))
            next_diff = (new_angle - angle) % 360

            if next_diff > 180:
                if 0 < chosen_diff <= 180:
                    chosen_x, chosen_y = next_x, next_y
                    chosen_diff = next_diff
                    continue

                can_min = min([7 - chosen_x, chosen_x, 7 - chosen_y, chosen_y])
                c_min = min([7 - next_x, next_x - 0, 7 - next_y, next_y])

                if all([
                    abs(last_y * 10 + 5 - oy) + abs(last_x * 10 + 5 - ox) < 15,
                    abs(next_y * 10 + 5 - oy) + abs(next_x * 10 + 5 - ox) < 15
                ]):
                    chosen_x, chosen_y = next_x, next_y
                    chosen_diff = next_diff
                elif c_min < can_min:
                    chosen_x, chosen_y = next_x, next_y
                    chosen_diff = next_diff
                elif can_min == c_min:
                    if (next_x, next_y) in ((0, 0), (0, 7), (7, 0), (7, 7)):
                        chosen_x, chosen_y = next_x, next_y

                    elif abs(delta_y) > abs(delta_x):
                        if delta_y > 0:
                            if next_y > chosen_y:
                                chosen_x, chosen_y = next_x, next_y
                                chosen_diff = next_diff
                            elif next_y == chosen_y and next_x > chosen_x:
                                chosen_x, chosen_y = next_x, next_y
                                chosen_diff = next_diff
                        else:
                            if next_y < chosen_y:
                                chosen_x, chosen_y = next_x, next_y
                                chosen_diff = next_diff
                            elif next_y == chosen_y and next_x < chosen_x:
                                chosen_x, chosen_y = next_x, next_y
                                chosen_diff = next_diff
                    else:
                        if delta_x > 0:
                            if next_x > chosen_x:
                                chosen_x, chosen_y = next_x, next_y
                                chosen_diff = next_diff
                            elif next_x == chosen_x and next_y > chosen_y:
                                chosen_x, chosen_y = next_x, next_y
                                chosen_diff = next_diff
                        else:
                            if next_x < chosen_x:
                                chosen_x, chosen_y = next_x, next_y
                                chosen_diff = next_diff
                            elif next_x == chosen_x and next_y < chosen_y:
                                chosen_x, chosen_y = next_x, next_y
                                chosen_diff = next_diff

        last_x, last_y = x, y
        x, y = chosen_x, chosen_y

    return board, x, y, False


count = 0


def traverse(board, num, x, y):
    global count

    if num == board_size*board_size:
        count += 1
        print('converged!!!')
        pprint.pprint(board)
        return

    possible_x = ((x + 1), (x + 2), (x + 2), (x + 1), (x - 1), (x - 2), (x - 2), (x - 1))
    possible_y = ((y - 2), (y - 1), (y + 1), (y + 2), (y + 2), (y + 1), (y - 1), (y - 2))
    possible_spaces = zip(possible_x, possible_y)

    def is_valid(space):
        return 0 <= space[0] < board_size and 0 <= space[1] < board_size and board[space[1]][space[0]] == '-'

    possible_spaces = list(filter(is_valid, possible_spaces))

    if not possible_spaces:
        # print('not converged!!!')
        return

    for (next_x, next_y) in possible_spaces:
        new_board = copy.deepcopy(board)
        new_board[next_y][next_x] = str(num + 1)
        traverse(new_board, num+1, next_x, next_y)


def brute_force():
    pause = 45
    board, x, y, status = lateralus(pause)
    if not status:
        print('not converged!!\n\n\n\n')
        return

    new_board = copy.deepcopy(board)
    traverse(new_board, pause, x, y)


brute_force()
print(f'{count} solutions found. completed!!!!\n\n\n\n')
