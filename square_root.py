"""
Calculate square root using long division method. Discard the decimal places.
Note: decimal places can be found in the similar manner by adding required number of zeros
"""


def square_root(x):
    if x in (0, 1):
        return x

    x_partial = []

    while x > 0:
        x_partial.append(x % 100)

        x = x // 100

    i = len(x_partial) - 1

    j = 0
    while True:
        if j ** 2 >= x_partial[i]:
            break

        if (j + 1) ** 2 > x_partial[i]:
            break

        j += 1

    quot = j

    rem = x_partial[i] - j ** 2
    i -= 1

    while i >= 0:
        rem = rem * 100 + x_partial[i]

        n = rem // (20 * quot)
        if n >= 10:
            n = 9

        calc = (20 * quot + n) * n

        if calc > rem:
            while calc > rem:
                n -= 1
                calc = (20 * quot + n) * n
        elif calc < rem:
            while calc < rem:
                n += 1
                c_calc = (20 * quot + n) * n

                if c_calc > rem:
                    n -= 1
                    break
                calc = c_calc

        quot = 10 * quot + n
        rem = rem - calc
        i -= 1

    return quot


print(square_root(25))
print(square_root(29))
print(square_root(1000000))
print(square_root(1000025))
