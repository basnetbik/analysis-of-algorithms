n = 5
C = 20

w = {
    1: 2,
    2: 4,
    3: 3,
    4: 7,
    5: 6
}

W = {
    (0, j): 0 for j in range(C+1)
}


def get_W(i, j):
    if j < 0:
        return -10000000000000000
    else:
        return W[(i, j)]

print 'W\t', '|\t', '\t'.join([str(i) for i in range(C+1)])
print '-'*n*19
for i in range(1, n+1):
    print 'i=%s\t' % str(i), '|\t',
    for j in range(C+1):
        W[(i, j)] = max([get_W(i-1, j), get_W(i-1, j-w[i])+w[i]])
        print W[(i, j)] , '\t',
    print
