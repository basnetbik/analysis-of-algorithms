n = 7
W = 30

# w = {
#     i: i for i in range(1, 10)
# }

w = {
    1: 4,
    2: 2,
    3: 1,
    4: 3,
    5: 2,
    6: 1,
    7: 2
}

v = {
    1: 1,
    2: 9,
    3: 2,
    4: 8,
    5: 4,
    6: 3,
    7: 6,
}

V = {
    (0, j): 0 for j in range(W+1)
}


def get_V(i, j):
    if j < 0:
        return -10000000000000000
    else:
        return V[(i, j)]

print 'W\t', '|\t', '\t'.join([str(i) for i in range(W+1)])
print '-'*n*19
for i in range(1, n+1):
    print 'i=%s\t' % str(i), '|\t',
    for j in range(W+1):
        old = get_V(i-1, j)
        new = get_V(i-1, j-w[i])+v[i]
        V[(i, j)] = max([get_V(i-1, j), get_V(i-1, j-w[i])+v[i]])
        if new > old:
            print '\033[92m' + str(new) + '\033[0m', '\t',
        else:
            print V[(i, j)], '\t',
    print



