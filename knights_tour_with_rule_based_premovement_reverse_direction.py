import copy
import pprint

board_size = 8


def pre_movement(pause=45):
    board = [['-'] * board_size for _ in range(board_size)]

    # possible_spaces1 = ([1, 2, 2, 1, -1, -2, -2, -1], [2, 1, -1, -2, -2, -1, 1, 2])
    # possible_spaces2 = ([-2, -1, 1, 2, 2, 1, -1, -2], [1, 2, 2, 1, -1, -2, -2, -1])
    # possible_spaces3 = ([-1, -2, -2, -1, 1, 2, 2, 1], [-2, -1, 1, 2, 2, 1, -1, -2])
    # possible_spaces4 = ([2, 1, -1, -2, -2, -1, 1, 2], [-1, -2, -2, -1, 1, 2, 2, 1])

    possible_spaces1 = ((2, 1, -1, -2, -2, -1, 1, 2), (1, 2, 2, 1, -1, -2, -2, -1))
    possible_spaces2 = ((-1, -2, -2, -1, 1, 2, 2, 1), (2, 1, -1, -2, -2, -1, 1, 2))
    possible_spaces3 = ((-2, -1, 1, 2, 2, 1, -1, -2), (-1, -2, -2, -1, 1, 2, 2, 1))
    possible_spaces4 = ((1, 2, 2, 1, -1, -2, -2, -1), (-2, -1, 1, 2, 2, 1, -1, -2))

    n = 1
    center = board_size * 5

    board_x = [['-'] * board_size for _ in range(board_size)]
    board_y = [['-'] * board_size for _ in range(board_size)]

    # x, y = 0, 0     # 26 solutions found
    # x, y = 0, 1     # 189 solutions found
    # x, y = 0, 2     # 14 solutions found
    # x, y = 0, 3     # 100 solutions found
    # x, y = 0, 4     # 17 solutions found
    # x, y = 0, 5     # 14 solutions found
    # x, y = 0, 6     # 255 solutions found
    # x, y = 0, 7     # 26 solutions found
    # x, y = 1, 0     # 255 solutions found
    # x, y = 1, 1     # 48 solutions found
    # x, y = 1, 2     # 0 solutions found
    # x, y = 1, 3     # 17 solutions found
    # x, y = 1, 4     # 7 solutions found
    # x, y = 1, 5     # 810 solutions found
    # x, y = 1, 6     # 48 solutions found
    # x, y = 1, 7     # 189 solutions found
    # x, y = 2, 0     # 14 solutions found
    # x, y = 2, 1     # 810 solutions found
    # x, y = 2, 2     # 1 solutions found
    # x, y = 2, 3     # 104 solutions found
    # x, y = 2, 4     # 53 solutions found
    # x, y = 2, 5     # 1 solutions found
    # x, y = 2, 6     # 0 solutions found
    # x, y = 2, 7     # 14 solutions found
    # x, y = 3, 0     # 17 solutions found
    # x, y = 3, 1     # 7 solutions found
    # x, y = 3, 2     # 53 solutions found
    # x, y = 3, 3     # 0 solutions found
    # x, y = 3, 4     # 0 solutions found
    # x, y = 3, 5     # 104 solutions found
    # x, y = 3, 6     # 17 solutions found
    # x, y = 3, 7     # 100 solutions found
    # x, y = 4, 0     # 100 solutions found
    # x, y = 4, 1     # 17 solutions found
    # x, y = 4, 2     # 104 solutions found
    # x, y = 4, 3     # 0 solutions found
    # x, y = 4, 4     # 0 solutions found
    # x, y = 4, 5     # 53 solutions found
    # x, y = 4, 6     # 7 solutions found
    # x, y = 4, 7     # 17 solutions found
    # x, y = 5, 0     # 14 solutions found
    # x, y = 5, 1     # 0 solutions found
    # x, y = 5, 2     # 1 solutions found
    x, y = 5, 3     # 53 solutions found
    # x, y = 5, 4     # 104 solutions found
    # x, y = 5, 5     # 1 solutions found
    # x, y = 5, 6     # 810 solutions found
    # x, y = 5, 7     # 14 solutions found
    # x, y = 6, 0     # 189 solutions found
    # x, y = 6, 1     # 48 solutions found
    # x, y = 6, 2     # 810 solutions found
    # x, y = 6, 3     # 7 solutions found
    # x, y = 6, 4     # 17 solutions found
    # x, y = 6, 5     # 0 solutions found
    # x, y = 6, 6     # 48 solutions found
    # x, y = 6, 7     # 255 solutions found
    # x, y = 7, 0     # 26 solutions found
    # x, y = 7, 1     # 255 solutions found
    # x, y = 7, 2     # 14 solutions found
    # x, y = 7, 3     # 17 solutions found
    # x, y = 7, 4     # 100 solutions found
    # x, y = 7, 5     # 14 solutions found
    # x, y = 7, 6     # 189 solutions found
    # x, y = 7, 7     # 26 solutions found

    for i in range(8):
        for j in range(8):
            board_x[i][j] = i * 10 + 5
            board_y[i][j] = j * 10 + 5

    def possible_spaces(p_spaces, x, y):
        for i in range(8):
            new_x = x+p_spaces[0][i]
            new_y = y+p_spaces[1][i]

            if 0 <= new_x < 8 and 0 <= new_y < 8 and board[new_y][new_x] == '-':
                return new_x, new_y, True

        return None, None, False

    while n <= 64:
        val = str(n)
        if len(val) == 1:
            val = f'0{val}'

        board[y][x] = val
        pprint.pprint(board)
        print('\n')

        if n == pause:
            return board, x, y, True

        if n == 64:
            print('converged!!!\n')
            break

        current_x = board_x[x][y] - center
        current_y = board_y[x][y] - center

        if current_x > 0 and current_y > 0:
            x, y, status = possible_spaces(possible_spaces1, x, y)
        elif current_x < 0 and current_y > 0:
            x, y, status = possible_spaces(possible_spaces2, x, y)
        elif current_x < 0 and current_y < 0:
            x, y, status = possible_spaces(possible_spaces3, x, y)
        else:
            x, y, status = possible_spaces(possible_spaces4, x, y)

        if not status:
            print('not converged!!!\n')
            return x, y, False

        n += 1


count = 0


def traverse(board, num, x, y):
    global count

    if num == board_size*board_size:
        count += 1
        print('converged!!!')
        pprint.pprint(board)
        return

    possible_x = ((x + 1), (x + 2), (x + 2), (x + 1), (x - 1), (x - 2), (x - 2), (x - 1))
    possible_y = ((y - 2), (y - 1), (y + 1), (y + 2), (y + 2), (y + 1), (y - 1), (y - 2))
    possible_spaces = zip(possible_x, possible_y)

    def is_valid(space):
        return 0 <= space[0] < board_size and 0 <= space[1] < board_size and board[space[1]][space[0]] == '-'

    possible_spaces = list(filter(is_valid, possible_spaces))

    if not possible_spaces:
        # print('not converged!!!')
        return

    for (next_x, next_y) in possible_spaces:
        new_board = copy.deepcopy(board)
        new_board[next_y][next_x] = str(num + 1)
        traverse(new_board, num+1, next_x, next_y)


def brute_force():
    pause = 45
    board, x, y, status = pre_movement(pause)

    if not status:
        print('not converged!!\n')
        return

    new_board = copy.deepcopy(board)
    traverse(new_board, pause, x, y)


brute_force()
print(f'{count} solutions found. completed!!!!\n\n\n\n')
