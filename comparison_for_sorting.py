import math
print "Solution"
print "Computing lower bound on number of comparisons for sorting for n=1...16"

for i in range(1,17):
	print "n = %i\t\t Lower bound (log(%d!) = %d"%(i,i,math.ceil(math.log(math.factorial(i),2)))