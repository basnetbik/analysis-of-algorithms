n, A = 4, 51

C = dict()

d = {
    1: 1,
    2: 5,
    3: 10,
    4: 12
}

for i in range(1, n+1):
    C[(i, 0)] = 0


for i in range(1, n+1):
    for j in range(1, A+1):
        try:
            old = C[(i-1, j)]
        except KeyError:
            old = A+100

        try:
            new = 1 + C[(i, j-d[i])]
        except KeyError:
            new = A + 100

        C[(i, j)] = min(old, new)


print 'A\t|\t'+ ''.join(map(lambda n: 'd'+str(n)+'\t', range(1, n+1)))
print '-'*100

for i in range(A+1):
    print str(i)+'\t|\t',
    for j in range(1, n+1):
        print str(C[(j,i)]) + '\t',
    print