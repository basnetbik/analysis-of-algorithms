data_list = [list('CATCHTWENTYTWO'), list('CATCHTHIRTYTHREE'), list('AQUICKBROWNFOXJUMPEDOVERTHELAZYDOGS')]


def merge(a, low, middle, high):
    buffer1 = a[low: middle+1]
    buffer2 = a[middle+1:high+1]

    i = low
    while len(buffer1) != 0 and len(buffer2) != 0:
        if buffer1[0] < buffer2[0]:
            a[i] = buffer1.pop(0)
        else:
            a[i] = buffer2.pop(0)

        i += 1

    while len(buffer1) != 0:
        a[i] = buffer1.pop(0)
        i += 1

    while len(buffer2) != 0:
        a[i] = buffer2.pop(0)
        i += 1


def merge_sort(a, low, high):
    if low < high:
        middle = int((low + high) / 2)
        merge_sort(a, low, middle)
        merge_sort(a, middle+1, high)
        merge(a, low, middle, high)


for data in data_list:
    merge_sort(data, 0, len(data) - 1)

    print(data)