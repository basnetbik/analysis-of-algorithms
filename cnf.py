class NCNF:
    expressions = []
    test_cases = ()

    def parse_e_(self, e_, case):
        if len(e_) == 1:
            return case
        elif len(e_) == 2:
            return not case

    def is_satisfiable(self, display=True):
        decision_result = False
        for test_case in self.test_cases:
            result = 1
            for e_ in self.expressions:
                partial_result = 0
                for index, e__ in enumerate(e_):
                    partial_result = partial_result or self.parse_e_(e__, test_case[index])

                result = result & partial_result

            if result == 1:
                decision_result = True

            if display:
                for case in test_case:
                    print case,
                print '|', result

        return decision_result


class UnsatisfiableNCNF(NCNF):
    def __init__(self, length):
        self.length = length

        self.variables = []
        self.set_variables()

        self.expressions = []
        self.expressions_pretty = ()
        self.set_expressions()

    def set_variables(self):
        last_variable = 122  # z
        variable_range = range(last_variable-self.length+1, last_variable+1)
        map(lambda ascii: self.variables.append(('!'+chr(ascii), chr(ascii))), variable_range)

    def set_expressions(self):
        temp_expressions = tuple(self.variables[0])

        for variable in self.variables[1:]:
            expressions_gathered = []
            for temp_exp in temp_expressions:
                expressions_gathered.append(' '.join((temp_exp, variable[0])))
                expressions_gathered.append(' '.join((temp_exp, variable[1])))

            temp_expressions = tuple(expressions_gathered)

        self.expressions_pretty = tuple(temp_expressions)

        temp_expressions = map(lambda _: tuple(_.split(' ')), self.expressions_pretty)
        self.test_cases = map(lambda _: [len(__) == 1 and 1 or 0 for __ in _], temp_expressions)
        self.expressions = tuple(temp_expressions)


def display_test_results(cnf, display):
    if display:
        # print cnf.variables
        print cnf.expressions_pretty
        # print cnf.expressions
        # print cnf.test_cases

    print cnf.is_satisfiable(display)
    print '----------------------------------------------'


# test unsatisfiable cnf
def test_unsatisfiable_n_cnf(range_, display):
    for i in range_:
        usat_i_cnf = UnsatisfiableNCNF(i)
        display_test_results(usat_i_cnf, display)

    print '----------------------------------------------'
    print


test_unsatisfiable_n_cnf(range(1, 5), display=True)


# test satisfiable cnf
def test_satisfiable_n_cnf(range_, display):
    for i in range_:
        sat_i_cnf = UnsatisfiableNCNF(i)
        sat_i_cnf.expressions = tuple(list(sat_i_cnf.expressions)[:-2])
        display_test_results(sat_i_cnf, display)

    print '----------------------------------------------'
    print

test_satisfiable_n_cnf(range(1, 5), display=True)