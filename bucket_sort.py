import random

from collections import defaultdict


def generate_data():
    """
    Generates random numbers (0,1500] with replacement
    :return:
    """
    data = random.sample(range(500, 1500), 900)
    data += random.sample(range(1000), 900)
    data += random.sample(range(200, 1200), 700)
    data += random.sample(range(400, 1400), 800)
    data += random.sample(range(100, 1100), 600)
    data += random.sample(range(300, 1300), 800)
    return data


def merge(a, low, middle, high):
    buffer1 = a[low: middle+1]
    buffer2 = a[middle+1:high+1]

    i = low
    while len(buffer1) != 0 and len(buffer2) != 0:
        if buffer1[0] < buffer2[0]:
            a[i] = buffer1.pop(0)
        else:
            a[i] = buffer2.pop(0)

        i += 1

    while len(buffer1) != 0:
        a[i] = buffer1.pop(0)
        i += 1

    while len(buffer2) != 0:
        a[i] = buffer2.pop(0)
        i += 1


def merge_sort(a, low, high):
    if low < high:
        middle = int((low + high) / 2)
        merge_sort(a, low, middle)
        merge_sort(a, middle+1, high)
        merge(a, low, middle, high)


def bucket_sort(root_bucket, result_):
    """
    Useful if the range of the numbers is very large with numerous repetitions of the numbers
    :param root_bucket: array to be sorted
    :param result_: sorted numbers
    :return:
    """
    buckets = {
        0: [],
        1: [],
        2: []
    }

    for datum in root_bucket:
        if 0 <= datum < 500:
            buckets[0].append(datum)
        elif 500 <= datum < 1000:
            buckets[1].append(datum)
        elif 1000 <= datum < 1500:
            buckets[2].append(datum)

    index = 0
    buckets_keys = list(buckets.keys())
    merge_sort(buckets_keys, 0, len(buckets_keys)-1)

    for bucket in buckets_keys:
        count = defaultdict(int)
        data_to_be_sorted = list()
        hash_to_prevent_duplicate = defaultdict(lambda: False)

        for datum in buckets[bucket]:
            count[datum] += 1
            if not hash_to_prevent_duplicate[datum]:
                hash_to_prevent_duplicate[datum] = True
                data_to_be_sorted.append(datum)

        merge_sort(data_to_be_sorted, 0, len(data_to_be_sorted) - 1)

        for datum in data_to_be_sorted:
            for n in range(count[datum]):
                result_[index] = datum
                index += 1

data = generate_data()
result = [0] * len(data)


bucket_sort(data, result)

print(result)