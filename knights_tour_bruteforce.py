import copy
import pprint


board_size = 6
count = 0


def traverse(board, num, x, y):
    global count

    if num == board_size*board_size:
        count += 1
        print('converged!!!')
        pprint.pprint(board)
        return

    possible_x = ((x + 1), (x + 2), (x + 2), (x + 1), (x - 1), (x - 2), (x - 2), (x - 1))
    possible_y = ((y - 2), (y - 1), (y + 1), (y + 2), (y + 2), (y + 1), (y - 1), (y - 2))
    possible_spaces = zip(possible_x, possible_y)

    def is_valid(space):
        return 0 <= space[0] < board_size and 0 <= space[1] < board_size and board[space[1]][space[0]] == '-'

    possible_spaces = list(filter(is_valid, possible_spaces))

    if not possible_spaces:
        # print('not converged!!!')
        return

    for (next_x, next_y) in possible_spaces:
        new_board = copy.deepcopy(board)
        new_board[next_y][next_x] = num + 1
        traverse(new_board, num+1, next_x, next_y)


def brute_force():
    val = 1
    board = [['-'] * board_size for _ in range(board_size)]

    for i in range(board_size):
        for j in range(board_size):
            new_board = copy.deepcopy(board)
            new_board[j][i] = val
            traverse(copy.deepcopy(new_board), val, i, j)


brute_force()
print(f'{count} solutions found. completed!!!!\n\n\n\n')
