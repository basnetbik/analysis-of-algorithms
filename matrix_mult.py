M, S = dict(), dict()

N = 5
d = {
    0: 21,
    1: 3,
    2: 18,
    3: 7,
    4: 33,
    5: 8
}

for i in range(1, N+1):
    M[(i, i)] = 0

cells = [(1, 2), (2, 3), (3, 4), (4, 5), (1, 3), (2, 4), (3, 5), (1, 4), (2, 5), (1, 5)]

for cell in cells:
    (i, j) = cell
    minimum = None
    min_k = None
    for k in range(i, j):
        current_value = M[(i, k)] + M[k+1, j] + d[i-1] * d[k] * d[j]

        if not minimum:
            minimum = current_value
            min_k = k
        elif minimum > current_value:
            minimum = current_value
            min_k = k

    M[(i, j)] = minimum
    S[(i, j)] = min_k


print M
print S

parenthesis = [1,2,3,4,5]


def calc_paren(current):
    if len(current) == 1:
        return current[0]

    split_value = S[(current[0], current[-1])]
    split_point = current.index(split_value)

    left = calc_paren(current[:split_point+1])
    right = calc_paren(current[split_point+1:])

    return [left, right]

print calc_paren(parenthesis)
