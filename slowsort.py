from __future__ import division
import math

t = dict()
t[0] = 0
t[1] = 0

for i in range(2, 201):
    t[i] = t[i-1]+1+t[math.ceil(i/2)]+t[math.floor(i/2)]

for i in range(20, 201, 20):
    print i, t[i]
